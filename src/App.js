import { useState, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Banner from './components/Banner';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import { UserProvider } from './UserContext';

function App() {

   // Global state hook for the user information for validating if a user is logged in
    const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })

    // Function for clearing localStorage on logout
    const unsetUser = () => {
      localStorage.clear();
    }

    // Used to check if the user info is properly stored upon login and if the localStorage is cleared upon logout
    useEffect(() => {
      console.log(user);
      console.log(localStorage);
    }, [user])

    useEffect(() => {
      fetch("http://localhost:3000/users/details", {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
         
         if(data._id !== undefined) {

            // Sets the use state values with the user details upon successful login
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });

          // Sts the user state to the initial value 
         } else {
            setUser({
                id: null,
                isAdmin: null
            });
         }
          
      });
    }, []);
  
  return (
    <Router>
      <div>
        <AppNavbar />
        <div className="container mt-4">
        <Routes>
          <Route exact path="/" component={Home} />
          {/*<Route path="/tours" pages={Tours} />*/}
          <Route path="/login" pages={Login} />
          <Route path="/register" pages={Register} />
          <Route path="/logout" pages={Logout} />
         </Routes> 
        </div>
      </div>
    </Router>
  );
};

export default App;


