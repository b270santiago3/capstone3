const toursData = [
	{
		id: "tour01",
		name: "El Nido",
		description: "El Nido, in Palawan Island, is a pathway to paradise islands and beaches in the Philippines. It lives up to the motto: the paradise is here!",
		price: 2499,
		onOffer: true
	},
	{
		id: "tour02",
		name: "Cebu",
		description: "It is one of the country's largest cities and is a bustling port. Its harbour is provided by the sheltered strait between Mactan Island and the coast. The country's oldest settlement, it is also one of its most historic and retains much of the flavour of its long Spanish heritage.",
		price: 3999
		onOffer: true
	},
	{
		id: "tour03",
		name: "Siargao",
		description: "Is an island of nine municipalities in the province of Surigao del Norte. Known as the “Surfing Capital of the Philippines”, Siargao is mainly responsible for introducing surfing to the country. Apart from surfing, Siargao is also open to other activities such as cave explorations and rock climbing.",
		price: 4599,
		onOffer: true
	},
	{
		id: "tour04",
		name: "Bohol",
		description: "The home of the famous Chocolate Hills, Bohol is one of the most visited destinations in the Central Visayas region of the Philippines. The island province offers breathtaking spots for history buffs, beach lovers, and adrenaline junkies",
		price: 2899,
		onOffer: true
	},
	{
		id: "tour05",
		name: "Coron",
		description: "Is one of Palawan’s most popular beach and island destinations. This paradise-like getaway comprises of the eastern half of Busuanga Island, Coron Island, and 50 other islets within the vicinity, all of which are separate from the main Palawan island",
		price: 4699,
		onOffer: true
	},
	{
		id: "tour06",
		name: "Bicol",
		description: "Bicol is a destination suitable for all with its historical sites, eco-tourism and watersports. There is also the famous Mayon Volcano, Bulusan Volcano Natural Park, and numerous stunning waterfalls",
		price: 5999,
		onOffer: true
	}

]
export default toursData;
